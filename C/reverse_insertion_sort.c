void reverse_insertion_sort(int array, int n)
{
	int i;
	int j;
	int cursor;

	i = 1;
	while (i < n)
	{
		cursor = array[i];
		j = i - 1;
		while (j >= 0 && array[j] < cursor)
		{
			array[j + 1] = array[j];
			j--;
		}
		array[j + 1] = cursor;
		i++;
}
