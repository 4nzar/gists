#define typename(x) _Generic((x),                                \
  _Bool: "_Bool",                                                \
  unsigned char: "unsigned char",                                \
  char: "char",                                                  \
  signed char: "signed char",                                    \
  short int: "short int",                                        \
  unsigned short int: "unsigned short int",                      \
  int: "int",                                                    \
  unsigned int: "unsigned int",                                  \
  long int: "long int",                                          \
  unsigned long int: "unsigned long int",                        \
  long long int: "long long int",                                \
  unsigned long long int: "unsigned long long int",              \
  float: "float",                                                \
  double: "double",                                              \
  long double: "long double",                                    \
  char *: "pointer to char",                                     \
  void *: "pointer to void",                                     \
  int *: "pointer to int",                                       \
  _Bool *: "pointer to _Bool",                                   \
  unsigned char **: "pointer to unsigned char",                  \
  signed char *: "pointer to signed char",                       \
  short int *: "pointer to short int",                           \
  unsigned short int *: "pointer to unsigned short int",         \
  unsigned int *: "pointer to unsigned int",                     \
  long int *: "pointer to long int",                             \
  unsigned long int *: "pointer to unsigned long int",           \
  long long int *: "pointer to long long int",                   \
  unsigned long long int *: "pointer to unsigned long long int", \
  float *: "pointer to float",                                   \
  double *: "pointer to double",                                 \
  default: "other")
