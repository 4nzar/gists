import os
import ctypes
import sys
import re
import typing
import argparse
from enum import Enum
import platform
from datetime import datetime
from pathlib import Path

g_installpath: str = ""
g_host: str = ""
g_project: str = "."
g_config: str = ""
g_output: str = ""
g_builddir: str = "build"
g_sourcedir: str = ""
g_dir_exclude: list[str] = []
g_headerdir: str = ""
g_testdir: str = ""
g_filetype: str = ""
g_compiler: str = ""
g_flags: str = ""
g_files_header: list[str] = []
g_files_source: list[str] = []
g_files_test: list[str] = []
g_documentation: bool = False
g_isstatic: bool = False
g_isshared: bool = False
g_isexecutable: bool = False
g_verbose: bool = False

def set_compiler() -> None:
	global g_compiler
	if g_compiler:
		return;
	if g_filetype in [".c", ".s", ".asm", ".m", ".h"]:
		g_compiler = "gcc"
	elif g_filetype in [".s", ".asm"]:
		g_compiler = "as"
	elif g_filetype in [".cc", ".cpp", ".cxx", ".cp", ".mm", ".hh", ".hpp"]:
		g_compiler = "g++"
	elif g_filetype in [".f", ".f90", ".f95", ".f03", ".f15", ".for"]:
		g_compiler = "gfortran"
	elif g_filetype in [".go"]:
		g_compiler = "go build"
	elif g_filetype in [".swift"]:
		g_compiler = "swiftc"
	else:
		print(("File type '" + g_filetype + "' not supported yet. Set to gcc."))
		g_compiler = "gcc"

def set_flags(compiler: str) -> str:

	content: str = ""
	compiler_var_name: str = ""
	compiler_flags_name: str = ""

	if compiler == "g++":
		compiler_var_name = "CXX"
		compiler_flags_name = "CXXFLAGS"
	elif compiler == "gfortran":
		compiler_var_name = "FF"
		compiler_flags_name = "FFLAGS"
	elif compiler == "go build":
		compiler_var_name = "GC"
		compiler_flags_name = "GFLAGS"
	elif compiler == "swiftc":
		compiler_var_name = "SWIFTC"
		compiler_flags_name = "SWIFTFLAGS"
	else:
		compiler_var_name = "CC"
		compiler_flags_name = "CFLAGS"

	content += compiler_var_name + "\t\t\t= " + compiler + "\n"
	content += compiler_flags_name + "\t\t= " + g_flags

	if g_files_header:
		content += " -I$(HEADER_DIR)"

	content += "\n"
	return content

def get_extension(files: list[str]) -> list[str]:
	result: list[str] = []
	for file in files:
		filename, extension = os.path.splitext(file)
		if extension not in result:
			result.append(extension)
	return result

def get_root(filename: str) -> str:
	return os.path.splitext(filename)[0]

def is_hidden(filepath):
	name = os.path.basename(os.path.abspath(filepath))
	return name.startswith('.') or has_hidden_attribute(filepath)

def has_hidden_attribute(filepath):
	try:
		attrs = ctypes.windll.kernel32.GetFileAttributesW(unicode(filepath))
		assert attrs != -1
		result = bool(attrs & 2)
	except (AttributeError, AssertionError):
		result = False
	return result

def is_source(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			".c", 
			".cc", 
			".cpp", 
			".cxx", 
			".asm", 
			".s", 
			".go", 
			".f", 
			".f90", 
			".f95", 
			".f03", 
			".f15", 
			".for", 
			".m", 
			".mm",
			".swift"
		])):
		return True
	return False

def is_header(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			".h", 
			".hh", 
			".hpp", 
		])):
		return True
	return False

def is_not_example(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			"_example.c", 
			"_example.cc", 
			"_example.cpp", 
			"_example.cxx", 
			"_example.asm", 
			"_example.s", 
			"_example.go", 
			"_example.f", 
			"_example.f90", 
			"_example.f95", 
			"_example.f03", 
			"_example.f15", 
			"_example.for", 
			"_example.m", 
			"_example.mm",
			"_example.swift",
			".example.c", 
			".example.cc", 
			".example.cpp", 
			".example.cxx", 
			".example.asm", 
			".example.s", 
			".example.go", 
			".example.f", 
			".example.f90", 
			".example.f95", 
			".example.f03", 
			".example.f15", 
			".example.for", 
			".example.m", 
			".example.mm",
			".example.swift"
		])):
		return False
	return True

def is_not_test(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			"_test.c", 
			"_test.cc", 
			"_test.cpp", 
			"_test.cxx", 
			"_test.asm", 
			"_test.s", 
			"_test.go", 
			"_test.f", 
			"_test.f90", 
			"_test.f95", 
			"_test.f03", 
			"_test.f15", 
			"_test.for", 
			"_test.m", 
			"_test.mm",
			"_test.swift",
			".test.c", 
			".test.cc", 
			".test.cpp", 
			".test.cxx", 
			".test.asm", 
			".test.s", 
			".test.go", 
			".test.f", 
			".test.f90", 
			".test.f95", 
			".test.f03", 
			".test.f15", 
			".test.for", 
			".test.m", 
			".test.mm",
			".test.swift",
			"_spec.c", 
			"_spec.cc", 
			"_spec.cpp", 
			"_spec.cxx", 
			"_spec.asm", 
			"_spec.s", 
			"_spec.go", 
			"_spec.f", 
			"_spec.f90", 
			"_spec.f95", 
			"_spec.f03", 
			"_spec.f15", 
			"_spec.for", 
			"_spec.m", 
			"_spec.mm",
			"_spec.swift"
			".spec.c", 
			".spec.cc", 
			".spec.cpp", 
			".spec.cxx", 
			".spec.asm", 
			".spec.s", 
			".spec.go", 
			".spec.f", 
			".spec.f90", 
			".spec.f95", 
			".spec.f03", 
			".spec.f15", 
			".spec.for", 
			".spec.m", 
			".spec.mm",
			".spec.swift"
		])):
		return False
	return True


def is_test(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			"_test.c", 
			"_test.cc", 
			"_test.cpp", 
			"_test.cxx", 
			"_test.asm", 
			"_test.s", 
			"_test.go", 
			"_test.f", 
			"_test.f90", 
			"_test.f95", 
			"_test.f03", 
			"_test.f15", 
			"_test.for", 
			"_test.m", 
			"_test.mm",
			"_test.swift",
			".test.c", 
			".test.cc", 
			".test.cpp", 
			".test.cxx", 
			".test.asm", 
			".test.s", 
			".test.go", 
			".test.f", 
			".test.f90", 
			".test.f95", 
			".test.f03", 
			".test.f15", 
			".test.for", 
			".test.m", 
			".test.mm",
			".test.swift",
			"_spec.c", 
			"_spec.cc", 
			"_spec.cpp", 
			"_spec.cxx", 
			"_spec.asm", 
			"_spec.s", 
			"_spec.go", 
			"_spec.f", 
			"_spec.f90", 
			"_spec.f95", 
			"_spec.f03", 
			"_spec.f15", 
			"_spec.for", 
			"_spec.m", 
			"_spec.mm",
			"_spec.swift"
			".spec.c", 
			".spec.cc", 
			".spec.cpp", 
			".spec.cxx", 
			".spec.asm", 
			".spec.s", 
			".spec.go", 
			".spec.f", 
			".spec.f90", 
			".spec.f95", 
			".spec.f03", 
			".spec.f15", 
			".spec.for", 
			".spec.m", 
			".spec.mm",
			".spec.swift"
		])):
		return True
	return False

def scan_project() -> list[str]:
	result: list[str] = []
	for path, subdirs, files in os.walk("."):
		subdirs[:] = [d for d in subdirs if d not in g_dir_exclude]
		for name in files:
			result.append(os.path.join(path, name))
	return result

def identify(files: list[str]) -> str:
	global g_filetype
	if not files:
		print("No files were found. Check if you are on the right directory.")
		exit(1)
	extension = get_extension(files)
	if len(extension) > 1:
		print("Multiple Language still not supported. Be sure to have same source extension")
		exit(1)
	g_filetype = extension[0]

class Args_description(Enum):
	verbose = "Enable verbose",
	compiler_type = "set compiler, if not set it will try to identify the compiler needed, if cannot it will set 'gcc' by default",
	compiler_flags = "set flags compiler, by default '-Wall -Wextra -Werror -std=c99 -pedantic-errors'",
	project_path = "Set project path, by default '.'",
	project_output = "Output filename, by default 'a.out'",
	documentation = "Set source directory if exist, by default '.'",
	build_type_static = "Build a static library",
	build_type_shared = "Build a shared library",
	build_type_executable = "Compile an executable",
	folder_build = "Set build directory, by default 'build'",
	folder_source = "Set source directory, by default '.'",
	folder_header = "Set header directory, by default '.'",
	folder_test = "set test directory, if not set, it will try to find itself all test files ending with (_test.<ext>, .test.<ext>,_spec.<ext>,.spec.<ext>)",
	folder_exlude = "directory to exclude to Makefile"

def get_options() -> None:
	global g_compiler,       \
			g_flags,         \
			g_verbose,       \
			g_sourcedir,     \
			g_headerdir,     \
			g_testdir,       \
			g_libdir,        \
			g_project,       \
			g_builddir,      \
			g_output,        \
			g_isshared,      \
			g_isstatic,      \
			g_isexecutable,  \
			g_documentation, \
			g_dir_exclude

	parser = argparse.ArgumentParser()
	parser.add_argument("--verbose", help=Args_description.verbose, action="store_true")
	parser.add_argument("--project", help=Args_description.project_path, default=".")
	parser.add_argument("--flags", help=Args_description.compiler_flags, default="-Wall -Wextra -Werror -std=c99 -pedantic-errors")
	parser.add_argument("-c", "--compiler", help=Args_description.compiler_type, default="")
	parser.add_argument("-o", "--output", help=Args_description.project_output, default="a.out")
	parser.add_argument("--documentation", help=Args_description.documentation, action="store_true")

	g = parser.add_mutually_exclusive_group(required=True)
	g.add_argument("--static-library", help=Args_description.build_type_static, action="store_true")
	g.add_argument("--shared-library", help=Args_description.build_type_shared, action="store_true")
	g.add_argument("--executable", help=Args_description.build_type_shared, action="store_true")

	parser.add_argument("--build-dir", help=Args_description.folder_build, default="build")
	parser.add_argument("--source-dir", help=Args_description.folder_source, default=".")
	parser.add_argument("--header-dir", help=Args_description.folder_header, default=".")
	parser.add_argument("--test-dir", help=Args_description.folder_test, default="")
	parser.add_argument("--exclude-dir", help="directory to exclude", action="append", default=[])

	args = parser.parse_args()

	g_compiler = args.compiler
	g_flags = args.flags
	g_verbose = args.verbose
	g_sourcedir = args.source_dir
	g_headerdir = args.header_dir
	g_testdir = args.test_dir
	#g_libdir = args.g_libdir
	g_project = args.project
	g_builddir = args.build_dir
	g_output = args.output
	g_isshared = args.shared_library
	g_isstatic = args.static_library
	g_isexecutable = args.executable
	g_documentation = args.documentation
	g_dir_exclude += args.exclude_dir

def generate_content() -> str:
	content = "# Generated " + datetime.now().strftime("%a %b %d %Y %H:%M:%S") + ", script written by ESSAIDI Sadik\n\n" 

	content += "NAME\t\t= " + g_output + "\n\n"

	content += set_flags(g_compiler)
	content += "SOURCE_EXT\t= " + g_filetype[1:] + "\n"
	content += "SOURCE_DIR\t= " + g_sourcedir + "\n"

	if g_files_header:
		content += "HEADER_DIR\t= " + g_headerdir

	if g_compiler in ["gcc", "g++", "gfortran", "swiftc"]:
		content += "\nBUILD_DIR\t= " + g_builddir

	if g_documentation:
		content += "\nDOC_DIR\t\t= doc"

	content += "\n\nSOURCES\t= " + "\nSOURCES	+= ".join(g_files_source) + "\n"

	if g_files_test:
		content += "\nTESTS\t= " + "\nTESTS	+= ".join(g_files_test) + "\n"

	if g_compiler in ["gcc", "g++"]:
		content += "\nOBJECTS = $(patsubst $(SOURCE_DIR)/%.o, $(BUILD_DIR)/%.o,$(SOURCES:.$(SOURCE_EXT)=.o))\n"

	content += "\n.PHONY: all\nall\t\t: $(NAME)\n"

	if g_compiler in ["gcc", "g++"]:
		content += set_rule_building_gxx()

	if g_compiler == "gobuild":
		content += set_rule_building_gxx()

	if g_compiler == "gfortran":
		content += set_rule_building_gxx()

	if g_compiler == "swiftc":
		content += set_rule_building_gxx()

	if g_isexecutable == True:
		content += "\n.PHONY: run\nrun\t\t: all\n\t./$(NAME)\n"

	if g_compiler in ["gcc", "g++"] and g_isstatic:
		content += set_rule_dynamic()

	if g_documentation:
		content += set_rule_documentation()

	if g_files_test:
		content += set_rule_test()

	content += "\n.PHONY: clean\nclean\t\t:\n"
	content += "\t@[ -f result-$(NAME).xml ] && rm result-$(NAME).xml || true;\n"
	content += "\t@[ -d $(BUILD_DIR) -a $(BUILD_DIR) != \".\" ] && /bin/rm -rvf $(BUILD_DIR) || true; \n"

	content += set_rule_fclean()

	if g_documentation:
		content += "\t@[ -d $(DOC_DIR) ] && /bin/rm -rf $(DOC_DIR) || true; \n"

	content += "\n.PHONY: re\nre\t\t: fclean all\n"
	return content

def set_rule_fclean() -> str:
	content: str = ""
	name: str = "$(NAME)"

	content += "\n.PHONY: fclean\nfclean\t"
	content += "\t: clean\n"
	if g_isstatic:
		name += ".a"
	else:
		if g_isshared:
			if g_host == "osx":
				name += ".dylib"
			if g_host == "linux":
				name += ".so"
	content += "\t@[ -f " + name + " ] && /bin/rm " + name + " || true; \n"
	return content

def set_rule_test() -> str:
	content: str = ""
	content += "\n.PHONY: test\ntest\t\t: re\n\t@[ ! -d .$@ ] && mkdir .$@ || true;\n"
	if g_compiler in ["gcc", "g++"]:
		content += set_rule_test_gxx()
	if g_compiler == "go build":
		content += set_rule_test_go()
	return content

def set_rule_test_gxx() -> str:
	content: str = ""
	content += ("\t@$(CXX) ","\t@$(CC) ")[g_compiler == "gcc"]
	if g_files_header:
		content += " -I$(HEADER_DIR)"
	content += " $(TESTS)"
	content += " $(OBJECTS) -lcriterion -o .$@/test_runner\n"
	content += "\t@.$@/test_runner -f --full-stats --xml=result-$(NAME).xml\n"
	content += "\t@[ -d .$@ ] && rm -rf .$@ || true;\n"
	return content

def set_rule_test_go() -> str:
	content: str = ""
	content += "\t@go test -v  -count=1 ./...\n"
	return content

def set_rule_documentation() -> str:
	content: str = ""
	content += "\n.PHONY: doc\ndoc\t\t:\n"
	content += "\t@[ ! -d $@ ] && mkdir $@ || true;\n"
	if g_compiler in ["gcc", "g++"]:

		content += "\t@if [ ! -f Doxyfile ]; \\\n"
		content += "\tthen \\\n"
		content += "\t\techo \"It seems that you don't have any Doxyfile available in your project.\"; \\\n"
		content += "\t\texit 1; \\\n"
		content += "\tfi\n"

		content += "\t@doxygen Doxyfile\n"
	if g_compiler == "go build":
		content += "\t@godoc -p config -r\n"
	return content

def set_rule_debug_gxx() -> str:
	content: str = ""
	return content

def set_rule_debug_go() -> str:
	content: str = ""
	return content

def set_rule_dynamic() -> str:
	content: str = ""
	content += "\n.PHONY: so\nso\t\t:\n"
	content += ("\t@$(CXX) ","\t@$(CC) ")[g_compiler == "gcc"]
	name: str = "$(NAME)"

	if g_host == "osx":
		content += "-dynamiclib -o $(NAME).dylib $(OBJECTS)\n"
		name += ".dylib"
	if g_host == "linux":
		content += "fPIC -shared -o $(NAME).so $(OBJECTS)\n"
		name += ".so"
	if g_host == "bsd":
		content += "fPIC -shared -o $(NAME).so $(OBJECTS)\n"
		name += ".so"
	return content

def set_rule_building_gxx() -> str:
	content: str = ""
	content += "\n$(NAME)\t\t: $(OBJECTS)\n"
	name: str = "$(NAME)"

	if g_isstatic:
		content += "\t@ar rcs $(NAME).a $(OBJECTS)\n"
		content += "\t@ranlib $(NAME).a\n"
		name += ".a"
	else:
		content += ("\t@$(CXX) ","\t@$(CC) ")[g_compiler == "gcc"]
		if g_isshared:
			if g_host == "osx":
				content += "-dynamiclib -o $(NAME).dylib $(OBJECTS)\n"
				name += ".dylib"
			if g_host == "linux":
				content += "-shared -o $(NAME).so $(OBJECTS)\n"
				name += ".so"
			if g_host == "bsd":
				content += "-shared -o $(NAME).so $(OBJECTS)\n"
				name += ".so"
		if g_isexecutable:
			content += " $(OBJECTS) -o $(NAME)\n"

	content += "\t@if [ -f " + name + " ]; \\\n\tthen \\\n\t\techo \"🌀 $(NAME) was successfully created!\"; \\\n"

	if g_files_test:
		content += "\t\techo \"🌀 type [\\033[1;36mmake test\\033[0m] to run tests\"; \\\n"

	if g_documentation:
		content += "\t\techo \"🌀 type [\\033[1;36mmake doc\\033[0m] to generate documentation\"; \\\n"

	content += "\tfi\n"

	content += "\n$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.$(SOURCE_EXT)\n"
	content += "\t@mkdir -p $(dir $@)\n"
	if g_compiler == "gcc":
		content += "\t@echo CC\t$(CFLAGS)"
	else:
		content += "\techo CXX\t$(CXXFLAGS)"
	content += " $@\n"
	if g_compiler == "gcc":
		content += "\t@$(CC) $(CFLAGS)"
	else:
		content += "\t@$(CXX) $(CXXFLAGS)"
	content += " -c $< -o $@\n"

	return content

def write_makefile(content: str):
	with open("Makefile", 'w') as makefile:
		makefile.write(content)
		makefile.close()

def identify_host():
	global g_host
	system = platform.uname().system
	if system == "Darwin":
		g_host = "osx"

def main():
	global g_files_header, \
			g_files_source, \
			g_files_test

	get_options()
	os.chdir(g_project)
	identify_host()
	files: list[str] = scan_project()

	g_files_source = list(filter(is_source, files))
	g_files_source = list(filter(is_not_example, g_files_source))
	g_files_source = list(filter(is_not_test, g_files_source))

	g_files_source = sorted([re.sub("^./" + g_sourcedir, "$(SOURCE_DIR)", o) for o in g_files_source])
	g_files_source = [ " ".join(g_files_source[i:i + 2]) for i in range(0, len(g_files_source), 2)] or []

	g_files_header = list(filter(is_header, files))
	g_files_header = sorted([re.sub("^./" + g_headerdir, "$(HEADER_DIR)", o) for o in g_files_header])

	g_files_test = sorted(list(filter(is_test, files)))
	g_files_test = [ " ".join(g_files_test[i:i + 2]) for i in range(0, len(g_files_test), 2)] or []

	if g_filetype == "":
		identify(g_files_source)
	set_compiler()
	write_makefile(generate_content())

if __name__ == "__main__":
	main()

